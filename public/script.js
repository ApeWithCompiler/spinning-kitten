const meowAudio = new Audio("/spinning-kitten/meow.mp3");

function playMeow() {
  meowAudio.play();
}

document.getElementById('cat').addEventListener('click', function (e) {
  playMeow();
});
